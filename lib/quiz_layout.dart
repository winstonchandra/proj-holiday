import 'package:flutter/material.dart';
import 'quiz_algorithm/number_convert.dart';
import 'quiz_algorithm/logic_gate.dart';
import 'quiz_algorithm/cypher_text.dart';
import 'end_game.dart';

class NumberConvertLayout extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NumberConvertState();
}

class GateLayout extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LogicGateState();
}

class CipherLayout extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CipherState();
}

class _NumberConvertState extends State<NumberConvertLayout> {
  final myController = TextEditingController();
  final NumberConvert num = NumberConvert();
  String _type = "";
  int _randomNumber = 0;
  int _playerScore = 0;
  String userAns = "";
  int _counter = 10;

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  _NumberConvertState() {
    _randomNumber = num.generateNumber();
    _type = num.initEssay();
  }

  void evalUserAnswer() {
    if (num.rightAnswer(myController.text)) {
      _playerScore += 10;
    }
    updateValue();
  }

  void updateValue() {
    _counter--;
    if (_counter > 0) {
      setState(() {
        _randomNumber = num.generateNumber();
        _type = num.initEssay();
      });
    } else {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => EndGame(_playerScore)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.blueAccent,
        body: Center(
          child: ListView(
            padding: EdgeInsets.all(20.0),
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 20.0),
                child: Text(
                  "\nPlayer Score: " + _playerScore.toString(),
                  style: TextStyle(
                    fontSize: 29.0,
                    color: Colors.orangeAccent,
                  ),
                ),
              ),
              Center(
                child: Text(
                  _randomNumber.toString(),
                  style: TextStyle(fontSize: 35.0),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 8.0),
                height: 100.0,
                padding: EdgeInsets.only(top: 8.0),
                child: Text(
                  _type + "\n\n",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 29.0, color: Colors.white70),
                ),
              ),
              Container(
                  padding: EdgeInsets.all(10.0),
                  margin: EdgeInsets.only(bottom: 25.0),
                  decoration: BoxDecoration(
                    color: Colors.white70,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  width: 300.0,
                  child: TextField(
                    controller: myController,
                    enabled: true,
                    style: TextStyle(fontSize: 28.0, color: Colors.black),
                  )),
              Container(
                  decoration: BoxDecoration(
                      color: Colors.black38,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  padding: EdgeInsets.all(5.0),
                  child: RawMaterialButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(fontSize: 24.0, color: Colors.white),
                    ),
                    onPressed: () {
                      userAns = myController.text;
                      evalUserAnswer();
                    },
                  ))
            ],
          ),
        ));
  }
}

class _LogicGateState extends State<GateLayout> {
  final LogicGate logic = new LogicGate();
  String quest = "";
  String userAns = "";
  int userScore = 0;
  int count = 0;

  void updateMethod() {
    count++;
    if (count < 10) {
      setState(() {
        quest = logic.initEssay();
      });
    } else {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => EndGame(userScore)));
    }
  }

  void evalUser() {
    if (logic.evalUserAns(userAns)) {
      userScore += 10;
    }
    updateMethod();
  }

  @override
  Widget build(BuildContext context) {
    logic.opsiJawaban.shuffle();
    quest = logic.initEssay();
    return Scaffold(
        backgroundColor: Colors.deepPurple,
        body: Center(
          child: ListView(
            padding: EdgeInsets.all(20.0),
            children: <Widget>[
              Text(
                "\n\nPlayer Score: " + userScore.toString(),
                style: TextStyle(fontSize: 29.0, color: Colors.deepOrangeAccent),
              ),
              Container(
                child: Text(
                  quest.toString(),
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 35.0, color: Colors.white70),
                ),
                margin: EdgeInsets.symmetric(vertical: 100.0),
              ),
              Container(
                  decoration: BoxDecoration(
                      color: Colors.black38,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  padding: EdgeInsets.all(5.0),
                  child: RawMaterialButton(
                    child: Text(
                      "A. " + logic.opsiJawaban[0],
                      style: TextStyle(fontSize: 24.0, color: Colors.white70),
                    ),
                    onPressed: () {
                      userAns = logic.opsiJawaban[0];
                      evalUser();
                    },
                  ),
                  margin: EdgeInsets.only(bottom: 10.0)),
              Container(
                  decoration: BoxDecoration(
                      color: Colors.black38,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  padding: EdgeInsets.all(8.0),
                  child: RawMaterialButton(
                    child: Text(
                      "B. " + logic.opsiJawaban[1],
                      style: TextStyle(fontSize: 24.0, color: Colors.white70),
                    ),
                    onPressed: () {
                      userAns = logic.opsiJawaban[1];
                      evalUser();
                    },
                  ),
                  margin: EdgeInsets.only(bottom: 10.0)),
              Container(
                  decoration: BoxDecoration(
                      color: Colors.black38,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  padding: EdgeInsets.all(8.0),
                  child: RawMaterialButton(
                    child: Text(
                      "C. " + logic.opsiJawaban[2],
                      style: TextStyle(fontSize: 24.0, color: Colors.white70),
                    ),
                    onPressed: () {
                      userAns = logic.opsiJawaban[2];
                      evalUser();
                    },
                  ),
                  margin: EdgeInsets.only(bottom: 10.0)),
              Container(
                  decoration: BoxDecoration(
                      color: Colors.black38,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  padding: EdgeInsets.all(8.0),
                  child: RawMaterialButton(
                    child: Text(
                      "D. " + logic.opsiJawaban[3],
                      style: TextStyle(fontSize: 24.0, color: Colors.white70),
                    ),
                    onPressed: () {
                      userAns = logic.opsiJawaban[3];
                      evalUser();
                    },
                  ))
            ],
          ),
        ));
  }
}

class _CipherState extends State<CipherLayout> {
  final control = TextEditingController();
  final CypherText cipher = new CypherText();
  int playerScore = 0;
  String quest = "";
  String userAns = "";
  int counter = 0;

  _CipherState() {
    quest = cipher.generateEssay();
  }

  @override
  void dispose() {
    control.dispose();
    super.dispose();
  }

  void updateState() {
    counter++;
    if (counter < 10) {
      setState(() {
        quest = cipher.generateEssay();
      });
    } else {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => EndGame(playerScore)));
    }
  }

  void evalUser() {
    if (cipher.rightAns(userAns)) {
      playerScore += 10;
    }
    updateState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.blueGrey,
        body: Center(
          child: ListView(
            padding: EdgeInsets.all(20.0),
            children: <Widget>[
              Text(
                "\nPlayer Score: " + playerScore.toString(),
                style: TextStyle(fontSize: 30.0, color: Colors.orangeAccent),
              ),
              Container(
                height: 100.0,
                margin: EdgeInsets.only(top: 25.0, bottom: 10.0),
                child: Text(
                  quest + "\n\n",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 29.0, color: Colors.white70),
                ),
              ),
              Container(
                  padding: EdgeInsets.all(10.0),
                  margin: EdgeInsets.only(bottom: 15.0, top: 18.0),
                  decoration: BoxDecoration(
                    color: Colors.white70,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  width: 300.0,
                  child: TextField(
                    controller: control,
                    enabled: true,
                    style: TextStyle(fontSize: 25.0, color: Colors.black),
                  )),
              Container(
                  decoration: BoxDecoration(
                      color: Colors.black38,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  padding: EdgeInsets.all(5.0),
                  child: RawMaterialButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(fontSize: 22.0, color: Colors.white),
                    ),
                    onPressed: () {
                      userAns = control.text;
                      evalUser();
                    },
                  ))
            ],
          ),
        ));
  }
}
