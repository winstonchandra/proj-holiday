import 'package:flutter/material.dart';
import 'quiz_layout.dart';

class PickGame extends StatelessWidget {
  final List<String> gameType = [
    "Number Convert",
    "Logic Gate",
    "Cipher Text"
  ];
  final List<Widget> buttonWidget = new List();
  BuildContext finalContext;

  @override
  Widget build(BuildContext context) {
    this.finalContext = context;
    return Scaffold(
        backgroundColor: Color(0xFF518A8A),
        body: Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: _createButton()),
        ));
  }

  List<Widget> _createButton() {
    gameType.forEach((string) {
      _addWidget(string);
    });
    return buttonWidget;
  }

  void _addWidget(String string) {
    if (string == "Number Convert") {
      buttonWidget.add(
          RawMaterialButton(
              onPressed: () {
                Navigator.pushReplacement(
                    finalContext,
                    MaterialPageRoute(builder: (finalContext) => NumberConvertLayout()));
              },
              child: Container(
                margin: EdgeInsets.only(top: 25.0),
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.deepOrangeAccent,
                ),
                child: Text(
                  string,
                  style: TextStyle(
                    fontSize: 35.0,
                    color: Colors.white,
                  ),
                ),
              ))
      );
    }
    else if (string == "Logic Gate") {
      buttonWidget.add(
          RawMaterialButton(
              onPressed: () {
                Navigator.pushReplacement(
                    finalContext,
                    MaterialPageRoute(builder: (finalContext) => GateLayout()));
              },
              child: Container(
                margin: EdgeInsets.only(top: 25.0),
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.deepOrangeAccent,
                ),
                child: Text(
                  string,
                  style: TextStyle(
                    fontSize: 35.0,
                    color: Colors.white,
                  ),
                ),
              ))
      );
    }
    else if (string == "Cipher Text") {
      buttonWidget.add(
          RawMaterialButton(
              onPressed: () {
                Navigator.pushReplacement(
                    finalContext,
                    MaterialPageRoute(builder: (finalContext) => CipherLayout()));
              },
              child: Container(
                margin: EdgeInsets.only(top: 25.0),
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.deepOrangeAccent,
                ),
                child: Text(
                  string,
                  style: TextStyle(
                    fontSize: 35.0,
                    color: Colors.white,
                  ),
                ),
              ))
      );
    }
  }
}
