import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'dart:math';

class CypherText extends StatelessWidget {
  final Random rnd = new Random();
  String word = "";
  int num1 = 0;
  String quest = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: Color(0xFF518A8A), body: Center());
  }

  String generateEssay(){
    quest = encryptCipher();
    String essay = quest+"\n\n"+
        "Decrypt it with key: -"+num1.toString();
    return essay;
  }

  bool rightAns(String ans) {
    return word == ans;
  }
  // Please check this method...
  String encryptCipher() {
    num1 = rnd.nextInt(26);
    word = rnd.nextInt(2) == 1 ?
        generateWordPairs().take(1).first.first.toString().toLowerCase() :
        generateWordPairs().take(1).last.first.toString().toLowerCase();
    num1 %= 26;
    String hasil = "";
    for (int i = 0; i < word.length; i++) {
      int ascii = word.codeUnitAt(i);
      if ((ascii >= 65) && (ascii <= 90)) {
        int convert = ascii - 65;
        int geser = (convert + num1) % 26;
        int hasilGeser = geser + 65;
        String temp = String.fromCharCode(hasilGeser);
        hasil += temp;
      }
      else if ((ascii >= 97) && (ascii <= 122)) {
        int convert = ascii - 97;
        int geser = (convert + num1) % 26;
        int hasilGeser = geser + 97;
        String temp = String.fromCharCode(hasilGeser);
        hasil += temp;
      }
    }
    return hasil;
  }

}