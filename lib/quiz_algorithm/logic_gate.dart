import 'package:flutter/material.dart';
import 'dart:math';

class LogicGate extends StatelessWidget {
  // Init instance for this class
  String ans = "";
  final List<String> gate = ["AND", "NAND", "OR", "XOR", "NOR", "XNOR", "NOT"];
  final List<String> opsiJawaban = ["Nol", "0", "Satu", "1"];
  List<String> listEssay = [];
  final Random rnd = new Random();
  String essay = "";

  // Methods area
  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: Color(0xFF518A8A), body: Center());
  }

  String initEssay() {
    do {
      String gateGetter = gate[rnd.nextInt(gate.length)];
      if (gateGetter == "NOT") {
        String num3 = rnd.nextInt(2).toString();
        essay = gateGetter + " " + num3;
      } else {
        String num1 = rnd.nextInt(2).toString();
        String num2 = rnd.nextInt(2).toString();
        essay = num1 + " " + gateGetter + " " + num2;
      }
    } while (this.listEssay.contains(essay));
    this.listEssay.add(essay);
    return essay;
  }

  bool evalUserAns(String playerAns) {
    String realAns = produceGate();
    return playerAns == realAns;
  }

  String produceGate() {
    List<String> arrInput = essay.split(" ");
    if (arrInput.length > 2) {
      if (arrInput[1] == "AND") {
        ans = (arrInput[0] == "1" && arrInput[2] == "1") ? "1" : "0";
      } else if (arrInput[1] == "NAND") {
        ans = (arrInput[0] == "1" && arrInput[2] == "1") ? "0" : "1";
      } else if (arrInput[1] == "OR") {
        ans = (arrInput[0] == "0" && arrInput[2] == "0") ? "0" : "1";
      } else if (arrInput[1] == "NOR") {
        ans = (arrInput[0] == "0" && arrInput[2] == "0") ? "1" : "0";
      } else if (arrInput[1] == "XOR") {
        ans = ((arrInput[0] == "1" && arrInput[2] == "0") ||
                (arrInput[0] == "0" && arrInput[2] == "1"))
            ? "1" : "0";
      } else if (arrInput[1] == "XNOR") {
        ans = ((arrInput[0] == "1" && arrInput[2] == "0") ||
                (arrInput[0] == "0" && arrInput[2] == "1"))
            ? "0" : "1";
      }
    } else {
      if (arrInput[0] == "NOT") {
        ans = (arrInput[1] == "1") ? "0" : "1";
      }
    }
    return ans;
  }
}
