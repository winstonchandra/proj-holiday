import 'package:flutter/material.dart';
import 'dart:math';

class NumberConvert extends StatelessWidget {
  final Map<int, String> hexAnomaly = {
    10: "A",
    11: "B",
    12: "C",
    13: "D",
    14: "E",
    15: "F"
  };

  int _number;
  String type;

  final Random random = Random.secure();
  List<String> orderOptions = [
    "Octal",
    "Unsigned Binary(0b)",
    "Hexadecimal(0x)"
  ];
  final List<int> usedInt = new List();

  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: Color(0xFF518A8A), body: Center());
  }

  String initEssay() {
    String ord = orderOptions[random.nextInt(orderOptions.length)];
    String ans = "Change it to " + ord;
    type = ord;
    return ans;
  }

  bool rightAnswer(String answer) {
    if (type == "Unsigned Binary(0b)") {
      return answer == intToUnsignedBinary();
    } else if (type == "Octal") {
      return answer == intToOctal();
    } else if (type == "Hexadecimal(0x)") {
      return answer == intToHex();
    } return false;
  }

  String intToUnsignedBinary() {
    String answer = "";
    while (_number > 0) {
      answer = (_number % 2).toString() + answer;
      _number = _number ~/ 2;
    }
    return answer;
  }

  String intToOctal() {
    String answer = "";
    while (_number > 0) {
      answer = (_number % 8).toString() + answer;
      _number = _number ~/ 8;
    }
    return answer;
  }

  String intToHex() {
    String answer = "";
    while (_number > 0) {
      String hexValue = hexAnomaly.containsKey(_number % 16)
          ? hexAnomaly[_number % 16]
          : (_number % 16).toString();
      answer = hexValue + answer;
      _number = _number ~/ 16;
    }
    return answer;
  }

  int generateNumber() {
    do {
      _number = random.nextInt(100);
    } while (usedInt.contains(_number));
    usedInt.add(_number);
    return _number;
  }
}
