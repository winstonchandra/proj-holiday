import 'package:flutter/material.dart';
import 'pick_game.dart';

class EndGame extends StatelessWidget {
  final int _score;

  EndGame(this._score);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: Center(
          child: Column(
              children: <Widget>[
        Container(
          padding: EdgeInsets.all(15.0),
            margin: EdgeInsets.symmetric(vertical: 185.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15.0)),
          color: Colors.greenAccent,
        ),
        child: Text(
          "Congratulations!\nYour score is: " + _score.toString(),
          style: TextStyle(fontSize: 40.0, color: Colors.deepOrangeAccent),
        ),
      ),
        Container(
            decoration: BoxDecoration(
                color: Colors.deepOrangeAccent,
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            padding: EdgeInsets.all(15.0),
            child: RawMaterialButton(
              child: Text(
                "Play Again?",
                style: TextStyle(fontSize: 24.0, color: Colors.white70),
              ),
              onPressed: () {
                Navigator.pushReplacement(
                    context, MaterialPageRoute(builder: (context) => PickGame()));
              },
            ))])),
    );
  }
}
