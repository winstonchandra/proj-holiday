import 'package:flutter/material.dart';
import 'pick_game.dart';

void main() => runApp(MaterialApp(home: MyApp(), debugShowCheckedModeBanner: false,));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xFF518A8A),
      body: Container(
        //decoration: _loginBackground(),
        child: ListView(
          children: <Widget>[
            Container(
              child: Image.asset("assets/logo_hawsop.png", width: 300.0),
              margin: EdgeInsets.only(top: 100.0, bottom: 150.0),
              alignment: Alignment.center,
            ),
            _playButton(context)
          ],
        ),
      ),
    );
  }

  Widget _playButton(BuildContext context) {
    return new RawMaterialButton(
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => PickGame()));
      },
      child: new Icon(
        Icons.play_arrow,
        size: 60.0,
        color: Colors.white,
      ),
      shape: new CircleBorder(),
      elevation: 2.0,
      fillColor: Colors.deepOrangeAccent,
      padding: const EdgeInsets.all(15.0),
    );
  }
}
